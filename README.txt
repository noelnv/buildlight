Build light.

This is a project about making a build light to monitor jenkins and XCode server builds.
Integration of lights with Jenkins is a solved problem, though I'm not aware of
anything which integrates with XCode server.

This project makes use of BlinkM RGB LED medules from ThingM (http://thingm.com/).

What you need:
* An arduino (I’m using an UNO, you can surely use other kinds, but this documentation is for an UNO)

* The Arduino IDE (I’m using 1.0.5)

* Fritzing (http://fritzing.org/) there are some diagrams for each sketch so you can see what's going on.

Steps for project development:

1. Address Each light.
Use the sketch BuildLightAddress to send commands to each light module to address it. See sketch comments for usage

Each light module has been marked on the bottom with a marker so we can tell each one apart.
I've addressed each module as follows:

Color	Address
----------------------
Green   0x09
Blue    0x0A
Black   0x0B

2. Control all three light modules with the Arduino independently via I2C interface using the three addresses defined above.
Use the sketch ThreeLights to test this.  We establish connectivity by making a sort of "network" between the devices, connecting each one up to Analog In 4 and 5.  See the accompanying Fritzing diagram for "schematics", and I do use this term loosely!

3. Use CmdMessenger arduino library to create a proper command-based firmware with callbacks to create a serial command interface to allow control of all three lights.

4. Build a wiring harness that allows the light modules to be connected without the use of a breadboard so that they can be installed into remote diffusers.

5. Develop software in the language of our choice to monitor both Jenkins and XCode server build statuses, and send proper serial commands to change the light different colors. (More details when hardware/firmware is working)

6: BONUS: use available blinkM sequencer software to create sweet-looking scripts so our build statuses can pulsate in bitchin patterns!  Adapt firmware to invoke these scripts instead of vanilla fade-to-color routines.