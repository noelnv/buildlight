/*
 * ThreeLights -- 
 *                   
 * BlinkM connections to Arduino
 * PWR - -- gnd -- black -- Gnd
 * PWR + -- +5V -- red   -- 5V
 * I2C d -- SDA -- green -- Analog In 4
 * I2C c -- SCK -- blue  -- Analog In 5
 *
 * Use this sketch to test independent control of three BlinkM modules.
 * Define the addresses of each light based on what you used in BuildLightAddress.
 * 
 * The sketch will verify that communication is working by blinking the lights red, green, and blue respectively
 * 
 * 2014, Nick Noel, IThinkStuff, 
 */

#include "Wire.h"
#include "BlinkM_funcs.h"

/*
=================================================================
SET the three addresses of the lights here
=================================================================
*/
#define blinkm_addr_1 0x09 //We'll turn this light Red
#define blinkm_addr_2 0x0A //We'll turn this light Green
#define blinkm_addr_3 0x0B //We'll turn this light Blue


/*
HELPER FUNCTIONS
*/
void turnRed(byte currAddress)
{
  BlinkM_setRGB(currAddress, 0xFF, 0x00, 0x00);
  delay(1000);
}

void turnBlue(byte currAddress)
{
  BlinkM_setRGB(currAddress, 0x00, 0x00, 0xFF);
  delay(1000);
}

void turnGreen(byte currAddress)
{
  BlinkM_setRGB(currAddress, 0x00, 0xFF, 0x00);
  delay(1000);
}

void turnOff(byte currAddress)
{
  BlinkM_setRGB(currAddress, 0x00, 0x00, 0x00);
  delay(1000);
}

void setup()
{
  BlinkM_beginWithPower();
  delay(100); // wait a bit for things to stabilize
  BlinkM_off(0);  // turn everyone off
} 

void loop()
{  
  turnRed(blinkm_addr_1);
  turnOff(blinkm_addr_1);
  turnGreen(blinkm_addr_2);
  turnOff(blinkm_addr_2);
  turnBlue(blinkm_addr_3);
  turnOff(blinkm_addr_3);  
} 
