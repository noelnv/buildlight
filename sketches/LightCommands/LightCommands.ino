/*
 * LightCommands -- 
 *                   
 * BlinkM connections to Arduino
 * PWR - -- gnd -- black -- Gnd
 * PWR + -- +5V -- red   -- 5V
 * I2C d -- SDA -- green -- Analog In 4
 * I2C c -- SCK -- blue  -- Analog In 5
 *
 * Use this sketch as the main firmware for controlling the BlinkM modules.
 * This is a message based firmwhere where any program can send string commands to 
 * the Arduino and the lights will respond according to that API.
 *
 * Define the addresses of each light based on what you used in BuildLightAddress.
 * 
 * use baud rate 115200
 * 2014, Nick Noel, IThinkStuff, 
 */

#include "Wire.h"
#include "BlinkM_funcs.h"
#include <CmdMessenger.h>

// ------------------ G L O B A L  V A R I A B L E S ----------------------------------------------
/*
=================================================================
SET the three addresses of the lights here
=================================================================
*/
byte blink_addr[] =
{
  0x09, //ID = 0
  0x0A, //ID = 1
  0x0B //ID = 2
};

byte blink_addr_all = 0;

//Messaging
char field_separator = ',';
char command_separator = ';';
CmdMessenger cmdMessenger = CmdMessenger(Serial, field_separator, command_separator);
// Timeout handling
long timeoutInterval = 2000; // 2 seconds
long previousMillis = 0;
int counter = 0;

enum//these are device to PC commands only
{
  kCOMM_ERROR    = 000, // Lets Arduino report serial port comm error back to the PC (only works for some comm errors)
  kACK           = 001, // Arduino acknowledges cmd was received
  kARDUINO_READY = 002, // After opening the comm port, send this cmd 02 from PC to check arduino is ready
  kERR           = 003, // Arduino reports badly formatted cmd, or cmd not recognised
  // For the above commands, we just call cmdMessenger.sendCmd() anywhere we want in our Arduino program.
  kSEND_CMDS_END, // Mustnt delete this line
};

//These are PC to device commands
messengerCallbackFunction messengerCallbacks[] = 
{
  all_off,//4
  id_off,//5
  all_fade_speed,//6
  id_fade_speed,//7
  all_time_adj,//8
  id_time_adj,//9
  all_fade_rgb,//10
  id_fade_rgb,//11
  all_set_rgb,//12
  id_set_rgb,//13
  all_fade_random_rgb,//14
  id_fade_random_rgb,//15
  id_get_rgb,//16
  all_play_script,//17
  id_play_script,//18
  all_stop_script,//19
  id_stop_script,//20
  NULL
};

// ------------------ M A I N ----------------------------------------------
void setup()
{
  setupMessaging();
  setupLights();
}

void loop()
{
  doMessageLoop();
}

// ------------------ L O O P  F U N C T I O N S ----------------------------------------------
void doMessageLoop(){
  // Process incoming serial data, if any
  cmdMessenger.feedinSerialData();
  // handle timeout function, if any
  if (  millis() - previousMillis > timeoutInterval )
  {
    timeout();
    previousMillis = millis();
  }
  // Loop.
}

// ------------------ S E T U P  F U N C T I O N S----------------------------------------------
void setupMessaging(){
  // Listen on serial connection for messages from the pc
  // Serial.begin(57600);  // Arduino Duemilanove, FTDI Serial
  Serial.begin(115200); // Arduino Uno, Mega, with AT8u2 USB

  // cmdMessenger.discard_LF_CR(); // Useful if your terminal appends CR/LF, and you wish to remove them
  cmdMessenger.printLfCr();       // Make output more readable whilst debugging in Arduino Serial Monitor
  
  // Attach default / generic callback methods
  cmdMessenger.attach(kARDUINO_READY, arduino_ready);
  cmdMessenger.attach(unknownCmd);

  // Attach my application's user-defined callback methods
  attach_callbacks(messengerCallbacks);

  arduino_ready();

  // blink
  pinMode(13, OUTPUT);
}

void setupLights(){
  BlinkM_beginWithPower();
  delay(100); // wait a bit for things to stabilize
  BlinkM_off(blink_addr_all);  // turn everyone off
}

// ------------------ S E T U P  H E L P E R S ----------------------------------------------
//MESSAGING
void attach_callbacks(messengerCallbackFunction* callbacks)
{
  int i = 0;
  int offset = kSEND_CMDS_END;
  while(callbacks[i])
  {
    cmdMessenger.attach(offset+i, callbacks[i]);
    i++;
  }
}

void timeout()
{
  // blink
  if (counter % 2)
    digitalWrite(13, HIGH);
  else
    digitalWrite(13, LOW);
  counter ++;
}

void sendTrue(){
  cmdMessenger.sendCmd(kACK,"True");
}

// ------------------ C A L L B A C K  M E T H O D S -------------------------

// ------------------ D E F A U L T  C A L L B A C K S -----------------------
void arduino_ready()
{
  // In response to ping. We just send a throw-away Acknowledgement to say "im alive"
  cmdMessenger.sendCmd(kACK,"Arduino Ready");
}

void unknownCmd()
{
  // Default response for unknown commands and corrupt messages
  cmdMessenger.sendCmd(kERR,"Unknown Command");
}

// ------------------ C U S T O M  C A L L B A C K  I N T E R F A C E S -----------------------

void all_off(){
/**
ID: 4
ARGS: None - add devices will turn off
RETURNS: True - to indicate msg was received
*/
  BlinkM_off(blink_addr_all);  // turn everyone off
  cmdMessenger.sendCmd(kACK, true);
}
void id_off(){
/**
ID: 5
ARGS: int id - device id to turn off
RETURNS: True - to indicate msg was received
*/
  int id = cmdMessenger.readIntArg();
  blink_id_off(id);
  cmdMessenger.sendCmd(kACK, true);
}
void all_fade_speed(){
/**
ID: 6
ARGS: int fadeSpeed - the fade speed to set
RETURNS: True - to indicate msg was received

This command sets the rate at which color fading happens. It takes one argument that is the 
fade speed from 1-255. The slowest fading occurs when the fade speed is 1. To change 
colors instantly, set the fade speed to 255. A value of 0 is invalid and is reserved for a future 
“Smart Fade” feature. The default value is 15.
*/
  int fadeSpeed = cmdMessenger.readIntArg();
  BlinkM_setFadeSpeed(blink_addr_all, fadeSpeed);
  cmdMessenger.sendCmd(kACK, true);  
}
void id_fade_speed(){
/**
ID: 7
ARGS: int id - target device, int fadeSpeed - the fade speed to set
RETURNS: True - to indicate msg was received
*/
  int id = cmdMessenger.readIntArg();
  int fadeSpeed = cmdMessenger.readIntArg();
  blink_id_fadespeed(id, fadeSpeed);
  cmdMessenger.sendCmd(kACK, true);  
}
void all_time_adj(){
/**
ID: 8
ARGS: int timeAdj - the timeAdj to set
RETURNS: True - to indicate msg was received

This command adjusts the playback speed of a light script. It takes one byte as an argument, 
a signed number between -128 and 127. The argument is treated as an additive adjustment to 
all durations of the script being played.
A value of 0 resets the playback speed to the default.
*/
  int timeAdj = cmdMessenger.readIntArg();
  BlinkM_setTimeAdj(blink_addr_all, timeAdj);
  cmdMessenger.sendCmd(kACK, true);  
}
void id_time_adj(){
/**
ID: 9
ARGS: int id - target device, int timeAdj - the timeAdj to set
RETURNS: True - to indicate msg was received
*/
  int id = cmdMessenger.readIntArg();
  int timeAdj = cmdMessenger.readIntArg();
  blink_id_timeAdj(id, timeAdj);
  cmdMessenger.sendCmd(kACK, true);  
}
void all_fade_rgb(){
/**
ID: 10
ARGS: int r - red component, int g - green component, int b - blue component
RETURNS: True - to indicate msg was received
*/
  int r = cmdMessenger.readIntArg();
  int g = cmdMessenger.readIntArg();
  int b = cmdMessenger.readIntArg();
  BlinkM_fadeToRGB(blink_addr_all,r,g,b);
  cmdMessenger.sendCmd(kACK, true);  
}
void id_fade_rgb(){
/**
ID: 11
ARGS: int id - target device, int r - red component, int g - green component, int b - blue component
RETURNS: True - to indicate msg was received
*/
  int id = cmdMessenger.readIntArg();
  int r = cmdMessenger.readIntArg();
  int g = cmdMessenger.readIntArg();
  int b = cmdMessenger.readIntArg();
  blink_id_fade_rgb(id, r, g, b);
  byte rgb[3];
  blink_id_get_rgb(id, &rgb[0]);
  cmdMessenger.sendCmdStart(kACK);
  cmdMessenger.sendCmdArg(rgb[0]);
  cmdMessenger.sendCmdArg(rgb[1]);
  cmdMessenger.sendCmdArg(rgb[2]);
  cmdMessenger.sendCmdEnd();  
}
void all_set_rgb(){
 /**
ID: 12
ARGS: int id - target device, int r - red component, int g - green component, int b - blue component
RETURNS: True - to indicate msg was received
*/
  int r = cmdMessenger.readIntArg();
  int g = cmdMessenger.readIntArg();
  int b = cmdMessenger.readIntArg();
  BlinkM_setRGB(blink_addr_all,r,g,b);
  cmdMessenger.sendCmd(kACK, true);  
}
void id_set_rgb(){
/**
ID: 13
ARGS: int id - target device, int r - red component, int g - green component, int b - blue component
RETURNS: True - to indicate msg was received
*/
  int id = cmdMessenger.readIntArg();
  int r = cmdMessenger.readIntArg();
  int g = cmdMessenger.readIntArg();
  int b = cmdMessenger.readIntArg();
  blink_id_set_rgb(id, r, g, b);
  byte rgb[3];
  blink_id_get_rgb(id, &rgb[0]);
  cmdMessenger.sendCmdStart(kACK);
  cmdMessenger.sendCmdArg(rgb[0]);
  cmdMessenger.sendCmdArg(rgb[1]);
  cmdMessenger.sendCmdArg(rgb[2]);
  cmdMessenger.sendCmdEnd();
}
void all_fade_random_rgb(){
/**
ID: 14
ARGS: int r - red range, int g - green range, int b - blue range
RETURNS: True - to indicate msg was received
*/
  int r = cmdMessenger.readIntArg();
  int g = cmdMessenger.readIntArg();
  int b = cmdMessenger.readIntArg();
  BlinkM_fadeToRandomRGB(blink_addr_all, r,g,b);
  cmdMessenger.sendCmd(kACK, true); 
}
void id_fade_random_rgb(){
/**
ID: 15
ARGS: int id - target device, int r - red range, int g - green range, int b - blue range
RETURNS: True - to indicate msg was received
*/
  int id = cmdMessenger.readIntArg();
  int r = cmdMessenger.readIntArg();
  int g = cmdMessenger.readIntArg();
  int b = cmdMessenger.readIntArg();
  blink_id_fade_random(id, r, g, b);
  byte rgb[3];
  blink_id_get_rgb(id, &rgb[0]);
  cmdMessenger.sendCmdStart(kACK);
  cmdMessenger.sendCmdArg(rgb[0]);
  cmdMessenger.sendCmdArg(rgb[1]);
  cmdMessenger.sendCmdArg(rgb[2]);
  cmdMessenger.sendCmdEnd();
  
}
void id_get_rgb(){
/**
ID: 16
ARGS: int id - target device
RETURNS: int r, int g, int b - to give the rgb values at the selected address
*/
  int id = cmdMessenger.readIntArg();
  byte rgb[3];
  blink_id_get_rgb(id, &rgb[0]);
  cmdMessenger.sendCmdStart(kACK);
  cmdMessenger.sendCmdArg(rgb[0]);
  cmdMessenger.sendCmdArg(rgb[1]);
  cmdMessenger.sendCmdArg(rgb[2]);
  cmdMessenger.sendCmdEnd();
}

void all_play_script(){
/**
ID: 17
ARGS: int script id, int reps -number of repetitions, byte pos - script line position to start from
RETURNS: true to indicate that the message was received

This command will play the speciﬁed light script immediately, stopping any currently playing 
script. The command takes two bytes as arguments. The ﬁrst byte is the script id of the script 
to play. A list of the available scripts is below. The second argument is the number of repeats 
to play the script. A repeats value of 0 means play the script forever. The last argument is the 
script line number to start playing from. A value of 0 means play the script from the start.
To adjust the playback speed of a script that’s running, adjust the fade speed (“Set Fade 
Speed”, ‘f’) and time adjust (“Set Time Adjust”, ‘t’) to taste. Altering these values can 
greatly alter the lighting effect for the built-in light scripts.

*/
  int scriptId = cmdMessenger.readIntArg();
  int reps = cmdMessenger.readIntArg();
  int pos = cmdMessenger.readIntArg();
  BlinkM_playScript(blink_addr_all, scriptId, reps, pos);
  cmdMessenger.sendCmd(kACK, true); 
}
void id_play_script(){
/**
ID: 18
ARGS: int id - target device, int scriptId, int reps, int pos
RETURNS: true to indicate that the message was received
*/
  int id = cmdMessenger.readIntArg();
  int scriptId = cmdMessenger.readIntArg();
  int reps = cmdMessenger.readIntArg();
  int pos = cmdMessenger.readIntArg();
  blink_id_play_script(id, scriptId, reps, pos);
  cmdMessenger.sendCmd(kACK, true); 
}
void all_stop_script(){
/**
ID: 19
ARGS: none
RETURNS: true to indicate the message was received
*/
  BlinkM_stopScript(blink_addr_all);
  cmdMessenger.sendCmd(kACK, true); 
}
void id_stop_script(){
/**
ID: 20
ARGS: int id - target device
RETURNS: true to indicate the message was received
*/
  int id = cmdMessenger.readIntArg();
  blink_id_stop_script(id);
  cmdMessenger.sendCmd(kACK, true); 
}
// ------------------ C U S T O M  C A L L B A C K  I M P L E M E N T A T I O N S ------------------
void blink_id_off(int id){
  byte addr =  blinkAddrForId(id);
  BlinkM_off(addr);
}

void blink_id_fadespeed(int id, int fadeSpeed){
  byte addr =  blinkAddrForId(id);
  BlinkM_setFadeSpeed(addr, fadeSpeed); 
}

void blink_id_timeAdj(int id, int timeAdj){
  byte addr =  blinkAddrForId(id);
  BlinkM_setTimeAdj(addr, timeAdj);  
}

void blink_id_fade_rgb(int id, int r, int g, int b){
  byte addr =  blinkAddrForId(id);
  BlinkM_fadeToRGB(addr,r,g,b);
}

void blink_id_set_rgb(int id, int r, int g, int b){
  byte addr =  blinkAddrForId(id);
  BlinkM_setRGB(addr,r,g,b);
}

void blink_id_fade_random(int id, int rr, int rg, int rb){
  byte addr =  blinkAddrForId(id);
  BlinkM_fadeToRandomRGB(addr, rr, rg, rb);
}

void blink_id_get_rgb(int id, byte *rgb){
  byte addr =  blinkAddrForId(id);
  BlinkM_getRGBColor(addr, &rgb[0], &rgb[1], &rgb[2]);
}

void blink_id_play_script(int id, int scriptId, int reps, int pos){
  byte addr =  blinkAddrForId(id);
  BlinkM_playScript(addr, scriptId, reps, pos);
}

void blink_id_stop_script(int id){
  byte addr = blinkAddrForId(id);
  BlinkM_stopScript(addr); 
}

// ------------------ E N D  C A L L B A C K  M E T H O D S ------------------

// ------------------ UTIL ------------------
byte blinkAddrForId(int id){
  int size = sizeof(blink_addr) / sizeof(byte);
  if(id < 0){
    return blink_addr[0];
  }
  if(id > size - 1){
    return blink_addr[0];
  }
  return blink_addr[id];
}
