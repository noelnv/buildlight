/*
 * BuildLightAddress -- 
 *                   
 * BlinkM connections to Arduino
 * PWR - -- gnd -- black -- Gnd
 * PWR + -- +5V -- red   -- 5V
 * I2C d -- SDA -- green -- Analog In 4
 * I2C c -- SCK -- blue  -- Analog In 5
 *
 * Use this sketch to set the address to a single BlinkM.
 * Define blinkm_addr below to define the address which will
 * be assigned.
 *
 * The sketch will verify that the address was set correctly by blinking the light red and green. 
 *
 * You can see what address was written by opening the serial monitor at 19200 baud.
 * 
 * 2014, Nick Noel, IThinkStuff, 
 */

#include "Wire.h"
#include "BlinkM_funcs.h"

/*
=================================================================
SET blinkm_addr TO BE THE ADDRESS YOU WANT TO GIVE TO THE DEVICE
=================================================================
*/
//#define blinkm_addr 0x09 //Green
//#define blinkm_addr 0x0A //Blue
#define blinkm_addr 0x0B //Black


/*
HELPER FUNCTIONS
*/
void turnRed(byte currAddress)
{
  BlinkM_setRGB(currAddress, 0xFF, 0x00, 0x00);
  delay(1000);
}

void turnOff(byte currAddress)
{
  BlinkM_setRGB(currAddress, 0x00, 0x00, 0x00);
  delay(1000);
}

void turnGreen(byte currAddress)
{
  BlinkM_setRGB(currAddress, 0x00, 0xFF, 0x00);
  delay(1000);
}

void setup()
{
  BlinkM_beginWithPower();
  delay(100); // wait a bit for things to stabilize
  BlinkM_off(0);  // turn everyone off
  BlinkM_setAddress( blinkm_addr );
  Serial.begin(19200);
} 

void loop()
{
  byte currAddress = BlinkM_getAddress(blinkm_addr);
  Serial.println( "Hi, my address is: ");
  Serial.print("Hex: 0x");  
  if(currAddress < 16){
    Serial.print("0");  
  }
  Serial.println( currAddress, HEX);
  Serial.print("Decimal: ");  
  Serial.println( currAddress, DEC);
  
  turnRed(currAddress);
  turnOff(currAddress);
  turnGreen(currAddress);
  turnOff(currAddress);
} 
