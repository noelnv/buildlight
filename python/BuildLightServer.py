class BuildLightServer:
    # BuildLightUSB and Flask
    def __init__(self, buildLight, flask):
        self.b = buildLight
        self.flask = flask

    # Two part post method
    def updateProject(self, target, update):
        if (self.b != None):
            self.b.update(target,update)
        return "Project " + target + " Updated to " + update

    # Setup the flask app with proper callbacks and host
    def go(self):
        self.flask.add_url_rule("/<string:target>/<string:update>",'updateProject',self.updateProject, methods=['POST'])
        self.flask.run()