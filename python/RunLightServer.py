from BuildLightServer import BuildLightServer
from TPCBuildLight import BuildLightUSB
from flask import Flask

# Setup Flask app for Web Hooks
app = Flask(__name__)

# Setup the USB hooks for the update
b = BuildLightUSB()
b.setup()

# Pass options to BuildLightServer to run
server = BuildLightServer(b, app)
server.go()