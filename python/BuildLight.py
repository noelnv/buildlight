import serial
import logging

logger = logging.getLogger('BuildLight')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler('BuildLight.log')
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)

USB_BAUD = 115200

USB_TIMEOUT = 5

STRINGS = {
    'CONNECTING': 'Connecting to BuildLight...',
    'READY': 'Arduino Ready',
    'ONLINE': 'BuildLight is online.',
    'OFFLINE': 'BuildLight failed to connect.',
    'TX': '-> ',
    'RX': '<- '
}


COMMANDS = {
    'COMM_ERROR': 0, 
    'ACK': 1, 
    'ARDUINO_READY': 2,
    'ERR': 3,
    'all_off': 4,
    'id_off': 5,
    'all_fade_speed': 6,
    'id_fade_speed': 7,
    'all_time_adj': 8,
    'id_time_adj': 9,
    'all_fade_rgb': 10,
    'id_fade_rgb': 11,
    'all_set_rgb': 12,
    'id_set_rgb': 13,
    'all_fade_random_rgb': 14,
    'id_fade_random_rgb': 15,
    'id_get_rgb': 16,
    'all_play_script': 17,
    'id_play_script': 18,
    'all_stop_script': 19,
    'id_stop_script': 20,
}

LIGHT_1 = 0
LIGHT_2 = 1
LIGHT_3 = 2

class BuildLight:
    def __init__(self, serialPort):
        # /dev/tty.usbmodem1411
        self.ser = serial.Serial(serialPort, USB_BAUD, timeout=USB_TIMEOUT)
        self.arduinoReady()
        
    def sendCommand(self, *args):
        cmdStr = ''

        for i in range(len(args)):
            cmdStr+=str(args[i])
            if i < (len(args) - 1):
                cmdStr+=','
            else:
                cmdStr+=';'

        logger.debug(STRINGS['TX'] + cmdStr)
        self.ser.write(cmdStr)
        return self.read()

    def read(self):
        respStr = self.ser.readline().strip()
        logger.debug(STRINGS['RX'] + respStr)
        valList = respStr[:-1].split(',')
        rcmd = valList[0]
        if(int(rcmd) == COMMANDS['ACK']):
            return valList[1:]
        else:
            logger.error(respStr)

    def arduinoReady(self):
        logger.info(STRINGS['READY'] + '?')
        result = self.sendCommand(COMMANDS['ARDUINO_READY'])
        if(result[0] == STRINGS['READY']):
            logger.info(STRINGS['ONLINE'])
        else:
            logger.info(STRINGS['OFFLINE'])
        return self.parse_result(result)

    def all_off(self):
        logger.info(self.all_off.__name__)
        result = self.sendCommand(COMMANDS['all_off'])
        logger.info(result)
        return self.parse_result(result)

    def id_off(self, lightId):
        logger.info(self.id_off.__name__)
        result = self.sendCommand(COMMANDS['id_off'], lightId)
        logger.info(result)
        return self.parse_result(result)

    def all_fade_speed(self, fadeSpeed):
        logger.info(self.all_fade_speed.__name__)
        result = self.sendCommand(COMMANDS['all_fade_speed'],fadeSpeed)
        logger.info(result)
        return self.parse_result(result)

    def id_fade_speed(self, lightId, fadeSpeed):
        logger.info(self.id_fade_speed.__name__)
        result = self.sendCommand(COMMANDS['id_fade_speed'],lightId, fadeSpeed)
        logger.info(result)
        return self.parse_result(result)

    def all_time_adj(self, timeAdj):
        logger.info(self.all_time_adj.__name__)
        result = self.sendCommand(COMMANDS['all_time_adj'], timeAdj)
        logger.info(result)
        return self.parse_result(result)
        
    def id_time_adj(self, lightId, timeAdj):
        logger.info(self.id_time_adj.__name__)
        result = self.sendCommand(COMMANDS['id_time_adj'], lightId, timeAdj)
        logger.info(result)
        return self.parse_result(result)

    def all_fade_rgb(self, rgb):
        logger.info(self.all_fade_rgb.__name__)
        result = self.sendCommand(COMMANDS['all_fade_rgb'],rgb[0],rgb[1],rgb[2])
        logger.info(result)
        return self.parse_result(result)

    def id_fade_rgb(self, lightId, rgb):
        logger.info(self.id_fade_rgb.__name__)
        result = self.sendCommand(COMMANDS['id_fade_rgb'], lightId, rgb[0], rgb[1], rgb[2])
        logger.info(result)
        return self.parse_result(result)

    def all_set_rgb(self, rgb):
        logger.info(self.all_set_rgb.__name__)
        result = self.sendCommand(COMMANDS['all_set_rgb'], rgb[0], rgb[1], rgb[2])
        logger.info(result)
        return self.parse_result(result)

    def id_set_rgb(self, lightId, rgb):
        logger.info(self.id_set_rgb.__name__)
        result = self.sendCommand(COMMANDS['id_set_rgb'], lightId, rgb[0], rgb[1], rgb[2])
        logger.info(result)
        return self.parse_result(result)

    def all_fade_random_rgb(self, rgbRanges,):
        logger.info(self.all_fade_random_rgb.__name__)
        result = self.sendCommand(COMMANDS['all_fade_random_rgb'], rgbRanges[0], rgbRanges[1], rgbRanges[2])
        logger.info(result)
        return self.parse_result(result)

    def id_fade_random_rgb(self, lightId, rgbRanges):
        logger.info(self.id_fade_random_rgb.__name__)
        result = self.sendCommand(COMMANDS['id_fade_rgb'], rgbRanges[0], rgbRanges[1], rgbRanges[2])
        logger.info(result)
        return self.parse_result(result)

    def id_get_rgb(self, lightId):
        logger.info(self.id_get_rgb.__name__)
        result = self.sendCommand(COMMANDS['id_get_rgb'], lightId)
        logger.info(result)
        return self.parse_result(result)

    def all_play_script(self, scriptId, reps=0, pos=0):
        logger.info(self.all_play_script.__name__)
        result = self.sendCommand(COMMANDS['all_play_script'], scriptId, reps, pos)
        logger.info(result)
        return self.parse_result(result)

    def id_play_script(self, lightId, scriptId, reps=0, pos=0):
        logger.info(self.id_play_script.__name__)
        result = self.sendCommand(COMMANDS['id_play_script'], lightId, scriptId, reps, pos)
        logger.info(result)
        return self.parse_result(result)

    def all_stop_script(self):
        logger.info(self.all_stop_script.__name__)
        result = self.sendCommand(COMMANDS['all_stop_script'])
        logger.info(result)
        return self.parse_result(result)

    def id_stop_script(self, lightId):
        logger.info(self.id_stop_script.__name__)
        result = self.sendCommand(COMMANDS['id_stop_script'], lightId)
        logger.info(result)
        return self.parse_result(result)

    def parse_result(self, result):
        if len(result) == 1 and result[0] == str(1):
            return True
        if len(result) == 3:
            vals = list()
            for i in result:
                vals.append(int(i))

            return tuple(vals)
        else:
            return result

