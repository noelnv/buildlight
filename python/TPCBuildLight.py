import os
from BuildLight import BuildLight

COLORS = {
  "red":(255,0,0),
  "green":(0,255,0),
  "blue":(0,0,255),
  "off":(0,0,0)
}

PROJECT = {
    "Mobile":0,
    "Web":2
}

STATUS = {
    "Success":"green",
    "Failed":"red",
    "Off":"off"
}

class BuildLightUSB:

    def setup(self):
        serial = modelCheck()
        self.b = BuildLight(serial)

    def update(self, project, status):
        projectID = PROJECT.get(project)
        if (projectID == None):
            print project + "Project not found"
            return
        statusColor = STATUS.get(status)
        statusRBG = COLORS.get(statusColor)
        if (statusRBG == None):
            print status + "Status Not Found"
            return
        self.b.id_set_rgb(projectID, statusRBG)

    def modelCheck():
        devList = os.listdir("/dev")

        aTarget = None
        for aItem in devList:
            if (aItem.find("tty.usbmodem") != -1):
                aTarget = aItem
                break
        if (aTarget != None):
            aTarget = "/dev/" + aTarget
        return aTarget;