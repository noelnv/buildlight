# Server to use without the hardware attached

from BuildLightServer import BuildLightServer
from flask import Flask

app = Flask(__name__)

server = BuildLightServer(None, app)
server.go()